# NSet

Nostr event implementation of the [`Set`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set) interface.

```ts
import { NSet } from 'https://gitlab.com/soapbox-pub/nstore/-/raw/main/mod.ts';

const set = new NSet();

set.add({ kind: 1, id: '123', ... });
set.size; // 1

set.clear();

// Only the newest version of replaceable events are stored.
set.add({ kind: 0, id: '456', author: 'abc' ... });
set.add({ kind: 0, id: '789', author: 'abc' ... });
set.size; // 1

set.clear();

// Kind 5 events delete their targets from the set, and prevent them from being added.
set.add({ kind: 1, author: 'abc', id: '123', ... });
set.add({ kind: 5, author: 'abc', tags: [['e', '123']], ... });
set.add({ kind: 1, author: 'abc', id: '123', ... });
set.size; // 1

// To match filters, loop the entire set.
for (const event of set) {
  if (matchFilters(filters, event)) {
    // do something
  }
}
```

Adding events with `NSet.add(event: NostrEvent)`:

- Events are stored by `id`.
- Replaceable events are replaced within the set. Older versions of replaceable events can't be added.
- Kind `5` events will delete their targets from the set. Those events can't be added, so long as the deletion event remains in the set.

Event validation is NOT performed. Callers MUST verify signatures before adding events to the set.

Any `Map` instance can be passed into `new NSet()`, making it compatible with [lru-cache](https://www.npmjs.com/package/lru-cache) and [ttl-cache](https://www.npmjs.com/package/@isaacs/ttlcache), among others:

```ts
import { NSet } from 'https://gitlab.com/soapbox-pub/nstore/-/raw/main/mod.ts';
import { LRUCache } from 'npm:lru_cache';
import { matchFilters, type NostrEvent } from 'npm:nostr-tools';

// Pass an LRUCache instance to NSet.
const cache = new NSet(
  new LRUCache<string, NostrEvent>({ max: 1000 }) as Map<string, NostrEvent>, // type assertion may be necessary
);

// Use the `NSet` as normal. Internally the LRUCache will evict old items.
cache.add({ kind: 1, id: '123', ... });

for (const event of cache) {
  if (matchFilters(filters, event)) {
    // when iterating the cache, you need to manually call `cache.get(event)` to indicate usage to the LRUCache.
    cache.get(event);

    // do something with `event`
  }
}
```

## License

MIT
